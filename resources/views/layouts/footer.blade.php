<?php
/**
 * Created by PhpStorm.
 * User: Rohit
 * Date: 9/15/2017
 * Time: 1:31 AM
 */?>

<!-- //footer -->
<div class="footer">
    <div class="container">
        <div class="footer-info w3-agileits-info">
            <div class="col-md-4 address-left agileinfo">
                <div class="footer-logo header-logo">
                    <h2><a href="index.html"><span>S</span>mart <i>Bazaar</i></a></h2>
                    <h6>Your stores. Your place.</h6>
                </div>
                <ul>
                    <li><i class="fa fa-map-marker"></i> 123 San Sebastian, New York City USA.</li>
                    <li><i class="fa fa-mobile"></i> 333 222 3333 </li>
                    <li><i class="fa fa-phone"></i> +222 11 4444 </li>
                    <li><i class="fa fa-envelope-o"></i> <a href="mailto:example@mail.com"> mail@example.com</a></li>
                </ul>
            </div>
            <div class="col-md-8 address-right">
                <div class="col-md-4 footer-grids">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="about.html">About Us</a></li>
                        <li><a href="marketplace.html">Marketplace</a></li>
                        <li><a href="values.html">Core Values</a></li>
                        <li><a href="privacy.html">Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-md-4 footer-grids">
                    <h3>Services</h3>
                    <ul>
                        <li><a href="contact.html">Contact Us</a></li>
                        <li><a href="login.html">Returns</a></li>
                        <li><a href="faq.html">FAQ</a></li>
                        <li><a href="sitemap.html">Site Map</a></li>
                        <li><a href="login.html">Order Status</a></li>
                    </ul>
                </div>
                <div class="col-md-4 footer-grids">
                    <h3>Payment Methods</h3>
                    <ul>
                        <li><i class="fa fa-laptop" aria-hidden="true"></i> Net Banking</li>
                        <li><i class="fa fa-money" aria-hidden="true"></i> Cash On Delivery</li>
                        <li><i class="fa fa-pie-chart" aria-hidden="true"></i>EMI Conversion</li>
                        <li><i class="fa fa-gift" aria-hidden="true"></i> E-Gift Voucher</li>
                        <li><i class="fa fa-credit-card" aria-hidden="true"></i> Debit/Credit Card</li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="copy-right">
    <div class="container">
        <p>© 2017 Kirana bazaar <a href="#"></a></p>
    </div>
</div>
<!-- cart-js -->
<script src="/assets/js/minicart.js"></script>
<script>
    w3ls.render();

    w3ls.cart.on('w3sb_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {
                items[i].set('shipping', 0);
                items[i].set('shipping2', 0);
            }
        }
    });
</script>
<!-- //cart-js -->
<!-- countdown.js -->
<script src="/assets/js/jquery.knob.js"></script>
<script src="/assets/js/jquery.throttle.js"></script>
<script src="/assets/js/jquery.classycountdown.js"></script>
<script>
    $(document).ready(function() {
        $('#countdown1').ClassyCountdown({
            end: '1388268325',
            now: '1387999995',
            labels: true,
            style: {
                element: "",
                textResponsive: .5,
                days: {
                    gauge: {
                        thickness: .10,
                        bgColor: "rgba(0,0,0,0)",
                        fgColor: "#1abc9c",
                        lineCap: 'round'
                    },
                    textCSS: 'font-weight:300; color:#fff;'
                },
                hours: {
                    gauge: {
                        thickness: .10,
                        bgColor: "rgba(0,0,0,0)",
                        fgColor: "#05BEF6",
                        lineCap: 'round'
                    },
                    textCSS: ' font-weight:300; color:#fff;'
                },
                minutes: {
                    gauge: {
                        thickness: .10,
                        bgColor: "rgba(0,0,0,0)",
                        fgColor: "#8e44ad",
                        lineCap: 'round'
                    },
                    textCSS: ' font-weight:300; color:#fff;'
                },
                seconds: {
                    gauge: {
                        thickness: .10,
                        bgColor: "rgba(0,0,0,0)",
                        fgColor: "#f39c12",
                        lineCap: 'round'
                    },
                    textCSS: ' font-weight:300; color:#fff;'
                }

            },
            onEndCallback: function() {
                console.log("Time out!");
            }
        });
    });
</script>

