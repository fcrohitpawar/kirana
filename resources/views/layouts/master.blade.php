<!DOCTYPE html>
<html>
<head>
    @include('layouts.head-scripts')
    @yield('head-content')
    @yield('page-style')
</head>
<body>
<header>
    @include('layouts.header')
    @include('layouts.sidebar')
</header>

<main>
    {{------------------------------------------CONTENT GOES HERE-----------------------------------------}}
    @yield('page-content')
</main>

@include('layouts.footer')
@include('layouts.footer-scripts')
@yield('page-scripts')
</body>
</html>