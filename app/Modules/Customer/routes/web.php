<?php

Route::group(['module' => 'Customer', 'middleware' => ['web'], 'namespace' => 'App\Modules\Customer\Controllers'], function() {

    Route::resource('Customer', 'CustomerController');

});
